/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import modelo.Empleado;

/**
 *
 * @author Oscar Marquez
 */
@Stateless
public class ControlEmpleado implements ControlEmpleadoLocal{
    @PersistenceContext(unitName="primerEjemploPU")
    EntityManager em;
    
    //creamos un metodo simple para persistir un empleado
    
    public void creaEmpleado (Empleado empleado){
                em.persist(empleado);
            }
            
}
