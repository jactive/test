package control;


import javax.ejb.Local;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Oscar Marquez
 */
@Local
public interface ControlEmpleadoLocal 
{    
    public void creaEmpleado (modelo.Empleado empleado);
}
