/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Oscar Marquez
 */
@XmlRootElement
@Entity
public class Departamento implements Serializable {
    @XmlElement
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="codDepartamento")
    private long codDepartamento;
    
    @XmlElement
    @Basic
    @Column (name="nombre")
    private String nombre;
    //Empleado tiene una relacion uno a muchos con Departamento, entonces, desde el punto de vista
    //de Departamento, un Departamento tiene muchos empleados
    
    @XmlElement
    @OneToMany(mappedBy="tiene")
    private List<Empleado> tiene;

    public long getCodDepartamento() {
        return codDepartamento;
    }

    public void setCodDepartamento(long codDepartamento) {
        this.codDepartamento = codDepartamento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Empleado> getTiene() {
        return tiene;
    }

    public void setTiene(List<Empleado> tiene) {
        this.tiene = tiene;
    }
 }
