/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Oscar Marquez
 */
@XmlRootElement
@Entity
public class Empleado implements Serializable{
    @XmlElement
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="codEmpleado")
    private long codEmpleado;
    
    @XmlElement
    @Basic
    @Column (name="nombre")
    private String nombre;
    
    @XmlElement
    @Basic
    @Column (name= "aPaterno")
    private String aPaterno;
    
    @XmlTransient
    @ManyToOne
    @JoinColumn (name="tiene", referencedColumnName="codDepartamento")
    private Departamento tiene;  

    public long getCodEmpleado() {
        return codEmpleado;
    }

    public void setCodEmpleado(long codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getaPaterno() {
        return aPaterno;
    }

    public void setaPaterno(String aPaterno) {
        this.aPaterno = aPaterno;
    }

    public Departamento getTiene() {
        return tiene;
    }

    public void setTiene(Departamento tiene) {
        this.tiene = tiene;
    }
}
