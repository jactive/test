/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import control.ControlEmpleadoLocal;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import modelo.Empleado;

/**
 *
 * @author Oscar Marquez
 */
@WebService(serviceName = "ControlEmpleadoWS")
public class ControlEmpleadoWS {
    @EJB
    private ControlEmpleadoLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "creaEmpleado")
    @Oneway
    public void creaEmpleado(@WebParam(name = "empleado") Empleado empleado) {
        ejbRef.creaEmpleado(empleado);
    }
    
}
